package org.springboard.neednetwork.users;

public class NormalUser extends BaseUser {
	private String normalUserMessage;

	public NormalUser(String msg) {
		super(msg);
		normalUserMessage = "I am normal user:";
	}
	
	public String getMessage() {
		return normalUserMessage + super.getMessage();
	}

}
