package org.springboard.neednetwork.users;

public abstract class BaseUser implements User{
	
	private String message;
	
	public BaseUser(String msg) {
		message = msg;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	protected void someMethodMeantForChildClass() {
		
	}
	

}
