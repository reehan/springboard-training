package org.springboard.neednetwork.users;

public class AdminUser extends BaseUser {
	private String adminMessage;

	public AdminUser(String msg) {
		super(msg);
		adminMessage = "I am admin:";
	}
	
	public String getMessage() {
		return adminMessage + super.getMessage();
	}
	
}
