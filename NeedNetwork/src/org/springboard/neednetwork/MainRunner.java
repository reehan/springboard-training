package org.springboard.neednetwork;

import org.springboard.neednetwork.users.AdminUser;
import org.springboard.neednetwork.users.NormalUser;
import org.springboard.neednetwork.users.User;
import org.springboard.neednetwork.utils.PrintHelper;

public class MainRunner {

	public static void main(String[] args) {
		User user;
		
		user = new AdminUser("Hello World");
		PrintHelper.printUserMessage(user);
		
		
		user = new NormalUser("Hello World");
		PrintHelper.printUserMessage(user);
	}

}
